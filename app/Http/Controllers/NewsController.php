<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(){

        $cardNews = [
            [
                "img" => "asset/news1.png",
                "date" => "27 august 2019",
                "title" => "Manchester United kembali menjuarai liga champions dan liga inggris berturut-turut",
                "desc" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque amet fuga voluptatibus ipsum? Et rerum excepturi quidem incidunt quod, sequi optio ratione eaque accusamus culpa autem maxime consequatur odit! Repellat?"
            ],
            [
                "img" => "asset/news1.png",
                "date" => "27 august 2019",
                "title" => "Manchester United kembali menjuarai liga champions dan liga inggris berturut-turut",
                "desc" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque amet fuga voluptatibus ipsum? Et rerum excepturi quidem incidunt quod, sequi optio ratione eaque accusamus culpa autem maxime consequatur odit! Repellat?"
            ],
            [
                "img" => "asset/news1.png",
                "date" => "27 august 2019",
                "title" => "Manchester United kembali menjuarai liga champions dan liga inggris berturut-turut",
                "desc" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque amet fuga voluptatibus ipsum? Et rerum excepturi quidem incidunt quod, sequi optio ratione eaque accusamus culpa autem maxime consequatur odit! Repellat?"
            ],
            [
                "img" => "asset/news1.png",
                "date" => "27 august 2019",
                "title" => "Manchester United kembali menjuarai liga champions dan liga inggris berturut-turut",
                "desc" => "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque amet fuga voluptatibus ipsum? Et rerum excepturi quidem incidunt quod, sequi optio ratione eaque accusamus culpa autem maxime consequatur odit! Repellat?"
            ],
        ];

        return view('news', compact('cardNews'));
    }
}
