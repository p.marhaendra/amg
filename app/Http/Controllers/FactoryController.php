<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FactoryController extends Controller
{
    public function index(){

        $factoryImg = [
            "asset/factory3.png",
            "asset/factory1.png",
            "asset/factory2.png",
            "asset/bg-news.jpg",
            "asset/bg-client.jpg",
            "asset/factory4.jpg",
        ];

        return view('factory', compact('factoryImg'));
    }
}
