@extends('layout.master') 

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/news.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-news.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">News & Blog</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>

        {{-- <div class="play-box uk-flex uk-flex-center">
            <img src="asset/icon-play.svg" alt="">
        </div> --}}
    </div>
</div>
    
@endsection

@section('content')

<div class="news-box">
        <div class="news-section">
            <div class="title-box">
                <h2 class="title-section">Lorem Ipsum Dolor <span>Sit Amet Proin Gravida Vel Avet</span></h2>
                <p class="bg1">News & Blog</p>                
            </div>
        </div>
    </div>

<div class="post-box">
    @foreach ($cardNews as $cardNewss)
    <div class="post-section uk-flex uk-flex-row">
        <div class="image-box">
            <div class="line2"></div>
            <img class="thumbnail" src={{$cardNewss['img']}} alt="">
        </div>
        <div class="detail-box">
            <p class="date">{{$cardNewss['date']}}</p>
            <p class="title">{{$cardNewss['title']}}</p>
            <p class="desc">{{$cardNewss['desc']}}</p>
            <button class="uk-button uk-button-default uk-flex uk-flex-middle uk-flex-center"> 
                Send 
                <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                <img class="arrow2" src="asset/arrow-right.svg" alt="">
            </button>
        </div>
    </div>
    @endforeach
</div>

@endsection
