<html>
    <head>
        <title>@yield('title')</title>
        <script src="uikit/dist/js/uikit.min.js"></script>
        <link rel="stylesheet" href="uikit/dist/css/uikit.min.css">
        @section('css')
        <link rel="stylesheet" href="css/master.css">
        @show
        <script src="uikit/dist/js/uikit-icons.min.js"></script>
    </head>
    <body>
        @section('jumbotron')
        @show

        @section('navbar')
        @show

        <div class="socmed-sticky uk-flex uk-flex-row">
            <div class="socmed-sticky-box uk-flex uk-flex-row">
                <img class="item-socmed-sticky" src="asset/ig.svg" alt="">
                <p class="item-socmed-sticky">Instagram</p>
                <img class="item-socmed-sticky line-sticky" src="asset/line-sticky.svg" alt="">
            </div>
            <div class="socmed-sticky-box uk-flex uk-flex-row">
                <img class="item-socmed-sticky fb" src="asset/fb.svg" alt="">
                <p class="item-socmed-sticky">Facebook</p>
                <img class="item-socmed-sticky line-sticky" src="asset/line-sticky.svg" alt="">
            </div>
        </div>
        
        <div class="container">
            @yield('content')
        </div>

        <div class="footer uk-flex uk-flex-center">
            <p>	&copy; 2019 Copyright by PT. AMG Printing, All rights reserved. Developed by <a href="">Alive</a></p>
        </div>

        {{-- modal --}}
        <div id="get-info" class="uk-modal-container" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <div class="box uk-flex uk-flex-center uk-flex-middle">
                    <div class="form-box">
                        <h1>Get In Touch With Us!</h1>
                        <form action="">
                            <input class="first-name" type="text" placeholder="Your First Name">
                            <input class="last-name" type="text" placeholder="Your Last Name">
                            <input class="phone" type="text" placeholder="Your Phone Number">
                            <input class="email" type="text" placeholder="Your Email">
                            <textarea class="textarea" name="" placeholder="Your Message" cols="30" rows="10"></textarea>
                            <button class="uk-modal-close-default close-btn uk-button uk-button-default uk-flex uk-flex-middle uk-flex-center"> 
                                    Close 
                                    <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                                    <img class="arrow2" src="asset/arrow-right.svg" alt="">
                                </button>
                            <button class="send-btn uk-button uk-button-default uk-flex uk-flex-middle uk-flex-center"> 
                                Send 
                                <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                                <img class="arrow2" src="asset/arrow-right.svg" alt="">
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="contact-us" class="uk-modal-container" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <div class="map-box">
                    <img src="asset/map.jpg" alt="">
                    {{-- <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe class="map" id="gmap_canvas" src="https://maps.google.com/maps?q=bxc&t=&z=13&ie=UTF8&iwloc=&output=embed" 
                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                        </iframe>
                        </div>
                    </div> --}}
                </div>
                
                <div class="form-box">
                    <form action="">
                        <input class="first-name" type="text" placeholder="Your First Name">
                        <input class="last-name" type="text" placeholder="Your Last Name">
                        <input class="phone" type="text" placeholder="Your Phone Number">
                        <input class="email" type="text" placeholder="Your Email">
                        <textarea class="textarea" name="" placeholder="Your Message" cols="30" rows="10"></textarea>
                        <div class="detail-box">
                            <div class="address">
                                <p>Salembaran Raya, Jl. Cilampe Pergudangan 2000, Blok A No 9-10 Kosambi, Tangerang</p>
                            </div>
                            <div class="contact">
                                <p>T, +62 21 559 313 96</p>
                                <p>F, +62 21 559 313 96</p>
                                <p>E, admin@amgprinting.com</p>
                            </div>
                        </div>
                        <button class="uk-button uk-button-default uk-flex uk-flex-middle uk-flex-center"> 
                            Send 
                            <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                            <img class="arrow2" src="asset/arrow-right.svg" alt="">
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <script>
            var toggleSearch = true;
            var nav = document.getElementById("nav");
            function search(){
                var searchBox = document.querySelector(".search-nav-box");

                if(this.toggleSearch == true){
                    searchBox.style.width = "150px";
                    searchBox.style.opacity = 1;
                    // nav.style.padding = "0 2%";
                    this.toggleSearch = false;
                } else{
                    searchBox.style.width = "0px";                    
                    searchBox.style.opacity = 0;
                    // nav.style.padding = "0 5%";
                    this.toggleSearch = true;
                }
            }
        </script>
    </body>
</html>