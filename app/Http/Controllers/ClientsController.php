<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index(){

        $clientLogo = [
            "asset/client4.png",
            "asset/client3.png",
            "asset/client2.png",
            "asset/client1.png",
            "asset/client4.png",
            "asset/client3.png",
            "asset/client2.png",
            "asset/client1.png",
        ];

        return view('clients', compact('clientLogo'));
    }
}
