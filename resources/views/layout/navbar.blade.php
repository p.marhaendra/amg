<div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
    <nav class="uk-navbar-container uk-navbar-transparent"  uk-navbar style="position: relative; z-index: 980;">
        <div class="uk-navbar-left">
            <a class="uk-navbar-item uk-logo" href="/">
                <img class="brand-logo" src="asset/logo.png" alt="">
            </a>
    
            <ul class="uk-navbar-nav">
                <li class="first-child-nav">
                    <a class="parent menu-item" href="#">
                        About Us
                        <img src="asset/arrow-down.svg" alt=""> 
                    </a>
                    <div class="uk-navbar-dropdown">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                            <li class="child-nav uk-active"><a href="/about">Our Company</a></li>
                            <li class="child-nav"><a href="/ourapproach">Our Approach</a></li>
                        </ul>
                    </div>
                </li>
                <li class="first-child-nav">
                    <a class="parent menu-item" href="/services">
                        Services
                        <img src="asset/arrow-down.svg" alt=""> 
                    </a>
                    <div class="uk-navbar-dropdown">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                            <li class="child-nav uk-active"><a href="/servicesdetail">DISCHARGE</a></li>
                            <li class="child-nav uk-active"><a href="/servicesdetail">PLASTISOL</a></li>
                            <li class="child-nav uk-active"><a href="/servicesdetail">RUBBER</a></li>
                            <li class="child-nav uk-active"><a href="/servicesdetail">SPECIAL</a></li>
                            <li class="child-nav uk-active"><a href="/servicesdetail">SUBLIMATION</a></li>
                        </ul>
                    </div>
                </li>
                <li class="first-child-nav">
                    <a class="parent menu-item" href="#">
                        Our Factory
                        <img src="asset/arrow-down.svg" alt="">                         
                    </a>
                    <div class="uk-navbar-dropdown">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                            <li class="child-nav uk-active"><a href="/factory">Our Factory</a></li>
                            <li class="child-nav"><a href="/machinery">Our Machinery</a></li>
                        </ul>
                    </div>
                </li>
                <li class="first-child-nav"><a class="menu-item" href="/clients">Clients</a></li>
                {{-- <li><a class="menu-item" href="/news">News/Blog</a></li> --}}
                <li class="first-child-nav"><a class="menu-item" href="/contactus">Contact Us</a></li>
            </ul>
    
        </div>
        <div class="uk-navbar-right">
            <div class="uk-navbar-item">
                <button uk-toggle="target: #get-info" class="info-btn uk-button uk-button-default">
                    <img src="asset/icon-mail.svg" alt="">
                    Get Info</button>
            </div>
            <div class="uk-navbar-item search-nav-box">
                <input placeholder="search" type="text">
            </div>
            <div  class="uk-navbar-item">
                <div onclick="search()" class="nav-search uk-flex uk-flex-middle uk-flex-center">
                    <img src="asset/search.svg" alt="">
                </div>
            </div>
        </div>
    </nav>
</div>