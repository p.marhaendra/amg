@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/getinfo.css">

@section('navbar')
@include('layout.navbar')
    
@endsection

@section('content')

<div id="get-info" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <div class="box uk-flex uk-flex-center uk-flex-middle">
            <div class="form-box">
                <h1>Get In Touch With Us!</h1>
                <form action="">
                    <input class="first-name" type="text" placeholder="Your First Name">
                    <input class="last-name" type="text" placeholder="Your Last Name">
                    <input class="phone" type="text" placeholder="Your Phone Number">
                    <input class="email" type="text" placeholder="Your Email">
                    <textarea class="textarea" name="" placeholder="Your Message" cols="30" rows="10"></textarea>
                    <button class="close-btn uk-button uk-button-default uk-flex uk-flex-middle uk-flex-center"> 
                            Close 
                            <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                            <img class="arrow2" src="asset/arrow-right.svg" alt="">
                        </button>
                    <button class="send-btn uk-button uk-button-default uk-flex uk-flex-middle uk-flex-center"> 
                        Send 
                        <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
                        <img class="arrow2" src="asset/arrow-right.svg" alt="">
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection