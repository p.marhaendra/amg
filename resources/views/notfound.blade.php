@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/notfound.css">

@section('content')

<div class="not-found-box">
    <div class="image-box">
        <img src="asset/service-detail1.jpg" alt="">
    </div>
    <div class="msg-box uk-flex uk-flex-column uk-flex-center uk-flex-middle">
        <p class="number">404</p>
        <p class="oops">oops</p>
        <p class="desc">We can't find the page you are looking for</p>
        <button class="uk-button uk-button-default">
            Back to Home
            <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
            <img class="arrow2" src="asset/arrow-right.svg" alt="">
        </button>
    </div>
</div>


@endsection
