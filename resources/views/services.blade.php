@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/services.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-service.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">Our Services</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>

        {{-- <div class="play-box uk-flex uk-flex-center">
            <img src="asset/icon-play.svg" alt="">
        </div> --}}
    </div>
</div>
    
@endsection

@section('content')

<div class="image-box">
    <img src="asset/newsdetail1.jpg" alt="">
</div>

<div class="detail-box">
    <div class="title-box">
        <h3 class="title-section">What <span>We Offer</span></h3>
        <img class="line1" src="asset/home-line1.png" alt="">            
    </div>
    <div class="desc-box">
        <p class="desc">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quibusdam adipisci dolorem aut repellat consectetur voluptates asperiores cum laborum qui, neque esse veniam, at, consequuntur corrupti. Voluptatibus, nemo! Nulla, assumenda illo.</p>
        <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt natus vel, excepturi et iure, perferendis itaque labore unde mollitia, incidunt enim explicabo officia aliquid doloribus magni earum! Odit, aut et.</p>
        <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt natus vel, excepturi et iure, perferendis itaque labore unde mollitia, incidunt enim explicabo officia aliquid doloribus magni earum! Odit, aut et.</p>
        <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt natus vel, excepturi et iure, perferendis itaque labore unde mollitia, incidunt enim explicabo officia aliquid doloribus magni earum! Odit, aut et.</p>
    </div>
</div>

<div class="services-box uk-flex uk-flex-column">
    <div class="title-box">
        <h3 class="title-section">We offer the services <span>to help you work better</span></h3>
        <p class="bg1">Services</p>
    </div>
    <div class="card-box">
        @foreach ($cardService as $cardServices)
        <div class="card-section uk-flex">
            <img src={{$cardServices['img']}} alt="">                    
            <div class="card-detail">
                <p class="title">{{$cardServices['title']}}</p>
                <p class="desc">{{$cardServices['desc']}}</p>
            </div>
        </div>
        @endforeach
    </div>
</div>

@include('layout.question')

@endsection