<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(){
        $cardAbout = [
            [
                "img"=>"asset/icon-lamp.svg",
                "title"=>"Seeking For Innovation",
                "desc"=>"lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet"                
            ],
            [
                "img"=>"asset/icon-eco.svg",
                "title"=>"eco-friendly",
                "desc"=>"lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet"                
            ],
            [
                "img"=>"asset/icon-clock.svg",
                "title"=>"fast delivery",
                "desc"=>"lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet"
            ]
        ];
        return view("about", compact('cardAbout'));
    }
}
