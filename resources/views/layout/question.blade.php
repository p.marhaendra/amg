<div class="question-box">
    <h2>If you have any question, Let's Contact Us Now!</h2>
    <button uk-toggle="target: #contact-us" class="amg-button uk-button uk-button-default uk-flex uk-flex-row uk-flex-center uk-flex-middle">
        Contact us
        <img src="asset/arrow-right.svg" alt="">
    </button>
    <img class="bg" src="asset/bg-service-details.jpg" alt="">
</div>