@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/ourapproach.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-approach.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">Our Approach</h2>
        <p class="jumbo-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa architecto nisi assumenda excepturi ut cumque, impedit fugit, maxime, neque eaque animi aperiam sunt quibusdam voluptatibus.</p>
    </div>
</div>
    
@endsection

@section('content')
<div class="about-box">
    <div class="detail-box uk-flex uk-flex-column uk-flex-center">
        <div class="title-box">
            <p class="bg1">Approach</p>
            <h2 class="title-section">Lorem ipsum dolor <span> sit amet proin gravida vel avet</span></h2>
            <img class="line1" src="asset/home-line1.png" alt="">                        
        </div>
        <p class="desc">Lorem ipsum dolorz, sit amet consectetur adipisicing elit. Veniam nihil maiores accusantium quia, dolor, nesciunt rem corporis cumque fugiat rerum laborum sint, quibusdam aspernatur quisquam quae ut assumenda illo neque!</p>
        <p class="desc">Lorem ipsum dolorz, sit amet consectetur adipisicing elit. Veniam nihil maiores accusantium quia, dolor, nesciunt rem corporis cumque fugiat rerum laborum sint, quibusdam aspernatur quisquam quae ut assumenda illo neque!</p>
    </div>
    <div class="image-box">
        <img class="portrait" src="asset/news2.jpg" alt="">
        <div class="line2"></div>
    </div>
</div>

<div class="about-box">
    <div class="image-box">
        <img class="portrait" src="asset/service-detail1.jpg" alt="">
        <div class="line3"></div>
    </div>
    <div class="detail-box detail-box2 uk-flex uk-flex-column uk-flex-center">
        <div class="title-box">
            <h2 class="title-section">Lorem<span> Sit amet proin</span></h2>
            <img class="line1" src="asset/home-line1.png" alt="">                        
        </div>
        <p class="desc desc2">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam nihil maiores accusantium quia, dolor, nesciunt rem corporis cumque fugiat rerum laborum sint, quibusdam aspernatur quisquam quae ut assumenda illo neque!</p>
        <div class="fitur-box">
            <div class="item-box uk-flex uk-flex-row">
                <div class="img-box uk-flex uk-flex-center uk-flex-middle uk-flex-none">
                    <img src="asset/icon-recycle.svg" alt="">
                </div>
                <div class="item-section uk-flex-column">
                    <p class="item-title">Environmentally friendly screen prints</p>
                    <p class="item-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi aut nemo possimus cum odio nihil veniam, commodi nostrum nisi odit.</p>
                </div>
            </div>
            <div class="item-box uk-flex uk-flex-row">
                <div class="img-box uk-flex uk-flex-center uk-flex-middle uk-flex-none">
                    <img src="asset/icon-trash.svg" alt="">
                </div>
                <div class="item-section uk-flex-column">
                    <p class="item-title">Conducting waste system</p>
                    <p class="item-desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi aut nemo possimus cum odio nihil veniam, commodi nostrum nisi odit.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="quote-box">
    <div class="quote-wrapper">
        <p class="quote">We run an Environmentally friendly bussiness from raw materials to waste management system</p>
        <p class="petik1">"</p>
        <p class="petik2">"</p>
    </div>
</div>

@include('layout.question')

@endsection
