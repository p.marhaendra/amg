@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/search.css">

@section('jumbotron')
<div class="jumbotron uk-position-relative">
    <img class="jumbo-img" src="asset/bg-news.jpg" alt="">
    <div class="uk-position-top">
        @section('navbar')
        @include('layout.navbar')
    </div>
    <div class="jumbo-wrapper uk-flex uk-flex-column uk-flex-middle uk-position-center">
        <h2 class="jumbo-title">Hasil Pencarian</h2>
        <div class="search-box uk-flex uk-flex-row">
            <input placeholder="Lorem ipsum dolor" class="input-search" type="text">
            <div class="btn-search uk-flex uk-flex-row uk-flex-middle uk-flex-center">
                <img src="asset/white-search.svg" alt="">
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('content')

<div class="search-box">
    <h1 class="title-search">Hasil Pencarian dari "Lorem ipsum dolor"</h1>

    <div class="card-wrapper">
        <div class="card-box">
            <p class="title">Lorem ipsum, dolor sit amet consectetur adipisicing elit. In, optio.</p>
            <p class="desc"><span class="result">Lorem ipsum dolor</span> sit amet consectetur adipisicing elit. Corrupti vitae exercitationem cupiditate ex sunt laborum perspiciatis quidem sapiente aliquam? Reprehenderit maiores recusandae ex inventore tempore ratione illum, molestias saepe iure soluta, at sit necessitatibus! Nemo, ipsa eum quas perspiciatis placeat.</p>
        </div>
        <div class="card-box">
            <p class="title">Lorem ipsum, dolor sit amet consectetur adipisicing elit. In, optio.</p>
            <p class="desc"><span class="result">Lorem ipsum dolor</span> sit amet consectetur adipisicing elit. Corrupti vitae exercitationem cupiditate ex sunt laborum perspiciatis quidem sapiente aliquam? Reprehenderit maiores recusandae ex inventore tempore ratione illum, molestias saepe iure soluta, at sit necessitatibus! Nemo, ipsa eum quas perspiciatis placeat.</p>
        </div>
        <div class="card-box">
            <p class="title">Lorem ipsum, dolor sit amet consectetur adipisicing elit. In, optio.</p>
            <p class="desc"><span class="result">Lorem ipsum dolor</span> sit amet consectetur adipisicing elit. Corrupti vitae exercitationem cupiditate ex sunt laborum perspiciatis quidem sapiente aliquam? Reprehenderit maiores recusandae ex inventore tempore ratione illum, molestias saepe iure soluta, at sit necessitatibus! Nemo, ipsa eum quas perspiciatis placeat.</p>
        </div>
        <div class="card-box">
            <p class="title">Lorem ipsum, dolor sit amet consectetur adipisicing elit. In, optio.</p>
            <p class="desc"><span class="result">Lorem ipsum dolor</span> sit amet consectetur adipisicing elit. Corrupti vitae exercitationem cupiditate ex sunt laborum perspiciatis quidem sapiente aliquam? Reprehenderit maiores recusandae ex inventore tempore ratione illum, molestias saepe iure soluta, at sit necessitatibus! Nemo, ipsa eum quas perspiciatis placeat.</p>
        </div>
        <div class="card-box">
            <p class="title">Lorem ipsum, dolor sit amet consectetur adipisicing elit. In, optio.</p>
            <p class="desc"><span class="result">Lorem ipsum dolor</span> sit amet consectetur adipisicing elit. Corrupti vitae exercitationem cupiditate ex sunt laborum perspiciatis quidem sapiente aliquam? Reprehenderit maiores recusandae ex inventore tempore ratione illum, molestias saepe iure soluta, at sit necessitatibus! Nemo, ipsa eum quas perspiciatis placeat.</p>
        </div>
        <div class="card-box">
            <p class="title">Lorem ipsum, dolor sit amet consectetur adipisicing elit. In, optio.</p>
            <p class="desc"><span class="result">Lorem ipsum dolor</span> sit amet consectetur adipisicing elit. Corrupti vitae exercitationem cupiditate ex sunt laborum perspiciatis quidem sapiente aliquam? Reprehenderit maiores recusandae ex inventore tempore ratione illum, molestias saepe iure soluta, at sit necessitatibus! Nemo, ipsa eum quas perspiciatis placeat.</p>
        </div>
    </div>
</div>

@endsection
