@extends('layout.master')

@section('title', 'Homes')

@section('css')
<link rel="stylesheet" href="css/construction.css">

@section('content')

<div class="msg-box uk-flex uk-flex-column uk-flex-center uk-flex-middle">
    <div class="image-box uk-flex uk-flex-middle uk-flex-center">
        <img src="asset/icon-construction.svg" alt="">
    </div>
    <h1 class="title-section">Under <span>construction</span></h1>
    <p class="desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam obcaecati dolorum ipsam fuga similique alias cupiditate atque quas, dolore eaque!</p>
    <button class="uk-button uk-button-default">
        Read More
        <img class="arrow1" src="asset/arrow-right-white.svg" alt="">
        <img class="arrow2" src="asset/arrow-right.svg" alt="">
    </button>

    <img class="bg1" src="asset/bg-machine.jpg" alt="">
</div>

@endsection
